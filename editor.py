import fileinput
import os


xfwm4 = os.listdir()
filedata = None
for file in xfwm4:
    print(file)
    if file == any(['themerc', 'editor.py']):
        continue
    with open(file, 'r') as f:
        filedata = f.read()
    print(filedata)
    # Replace the target string
    n_filedata = filedata.replace('c gray10', 'c gray10')
    print(filedata)
    # Write the file out again
    with open(file, 'w') as f:
        f.write(n_filedata)
